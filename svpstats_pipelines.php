<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion dans le pipeline `affiche_gauche`.
 *
 * Affiche un bloc d'infos sur les statistiques d'utilisation des plugins et leur actualisation.
 *
 * @param array $flux Flux d'entrée
 *
 * @return array Flux mis à jour
 */
function svpstats_affiche_gauche(array $flux) : array {
	if ($flux['args']['exec'] == 'depots') {
		if (
			(sql_select('id_depot', 'spip_depots'))
			and ($texte = recuperer_fond('prive/squelettes/inclure/info_stats', []))
		) {
			if ($pos = strpos($flux['data'], '<!--affiche_gauche_milieu-->')) {
				$flux['data'] = substr_replace($flux['data'], $texte, $pos, 0);
			} else {
				$flux['data'] .= $texte;
			}
		}
	}

	return $flux;
}

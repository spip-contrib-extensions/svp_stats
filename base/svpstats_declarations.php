<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des nouvelles tables de la base de données propres au plugin.
 *
 * Le plugin déclare deux nouveaux champs dans la table spip_plugins, le nombre de sites utilisateurs et la popularité.
 *
 * @pipeline declarer_tables_principales
 *
 * @param array $tables Tableau global décrivant la structure des tables de la base de données
 *
 * @return array Tableau fourni en entrée et mis à jour avec les nouvelles déclarations
 */
function svpstats_declarer_tables_principales($tables) {
	// Ajout de champs dans la tables des plugins : spip_plugins
	$tables['spip_plugins']['field']['nbr_sites'] = 'integer DEFAULT 0 NOT NULL';
	$tables['spip_plugins']['field']['popularite'] = "double DEFAULT '0' NOT NULL";

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons).
 * Le plugin déclare 1 nouvelle table auxilliaire, celle des liens entre les plugins et leurs statistiques, `spip_plugins_stats`.
 *
 * @pipeline declarer_tables_auxiliaires
 *
 * @param array $tables Description des tables auxilliaires
 *
 * @return array Description complétée des tables auxilliaires
 */
function svpstats_declarer_tables_auxiliaires($tables) {
	// Tables de liens entre plugins et les stats : spip_plugins_stats
	$spip_plugins_stats = [
		'prefixe'      => "varchar(48) DEFAULT '' NOT NULL",
		'branche_spip' => "varchar(255) DEFAULT '' NOT NULL",
		'nbr_sites'    => 'integer DEFAULT 0 NOT NULL',
		'popularite'   => "double DEFAULT '0' NOT NULL",
		'historique'   => "text DEFAULT '' NOT NULL", // Historique mensuel
		'maj'          => 'TIMESTAMP',
	];

	$spip_plugins_stats_key = [
		'PRIMARY KEY' => 'prefixe, branche_spip'
	];

	$tables['spip_plugins_stats'] =
		['field' => &$spip_plugins_stats, 'key' => &$spip_plugins_stats_key];

	return $tables;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface Déclarations d'interface pour le compilateur
 *
 * @return array Déclarations d'interface mis à jour
 */
function svpstats_declarer_tables_interfaces(array $interface) : array {
	// Les tables
	$interface['table_des_tables']['plugins_stats'] = 'plugins_stats';

	// Les jointures
	// -- Entre spip_plugins_stats et spip_plugins
	$interface['tables_jointures']['spip_plugins'][] = 'plugins_stats';

	return $interface;
}

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-svpstats?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// S
	'svpstats_description' => 'Este plugin es un módulo opcional de SVP. 
_ Permite adquirir, actualizar y restituir las estadísticas de uso de los plugin SPIP en la red. 
Estas estadísticas son resultado del sitio <a href="http://stats.spip.org">stats.spip.org</a>.',
	'svpstats_slogan' => 'Módulo SVP de gestión de estadísticas de uso de los plugins',
];

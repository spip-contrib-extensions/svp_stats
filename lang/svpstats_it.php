<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/svpstats?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_actualiser_stats' => 'Aggiorna statistiche',
	'bulle_actualiser_stats' => 'Aggiorna le statistiche del plugin',

	// I
	'info_actualisation_stats_cron' => 'Le statistiche sull’utilizzo del plugin vengono aggiornate automaticamente ogni @periode@ giorno(i).',
	'info_boite_statistiques' => '<strong>Hai abilitato le statistiche sull’utilizzo del plug-in.</strong><p>Queste vengono aggiornate ogni @periode@ giorno(i). Tuttavia, puoi avviare un aggiornamento manuale in qualsiasi momento.</p>',
	'info_nbr_sites_0' => 'Utilizzato in qualsiasi sito',
	'info_nbr_sites_1' => 'Utilizzato in un sito',
	'info_nbr_sites_n' => 'Utilizzato in @nb@ siti',
];

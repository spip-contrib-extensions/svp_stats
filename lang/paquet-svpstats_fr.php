<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/svp_stats.git

return [

	// S
	'svpstats_description' => 'Ce plugin est un module optionnel de SVP. 
_ Il permet d’acquérir, de mettre à jour et de restituer les statistiques d’utilisation des plugins SPIP sur le net. 
Ces statistiques sont issues du site <a href="http://stats.spip.org">stats.spip.org</a>.',
	'svpstats_slogan' => 'Module SVP de gestion des statistiques d’utilisation des plugins',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-svpstats?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// S
	'svpstats_description' => 'Questo plugin è un modulo opzionale di SVP.
_ Consente di acquisire, aggiornare e ripristinare statistiche sull’utilizzo dei plugin SPIP in rete. 
Queste statistiche sono prese dal sito <a href="http://stats.spip.org">stats.spip.org</a>.',
	'svpstats_slogan' => 'Modulo di gestione delle statistiche di utilizzo del plug-in SVP',
];

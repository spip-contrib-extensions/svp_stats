<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-svpstats?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// S
	'svpstats_description' => 'Deze plugin is een optionele module van SVP. 
_ Je kunt ermee de gebruiksgegevens van SPIP plugins verzamelen, updaten en terugzetten. 
Deze statistieken komen van de site <a href="http://stats.spip.org">stats.spip.org</a>.',
	'svpstats_slogan' => 'SVP module voor het beheer van statistieken over het gebruik van plugins',
];

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Retourner le nombre de jours entre chaque actualisation des stats
 * si le cron est activé.
 *
 * @return int Nb de jours (sinon 0)
 */
function filtre_svpstats_periode_actualisation_stats() : int {
	include_spip('genie/svpstats_taches_generales_cron');
	return _SVP_CRON_ACTUALISATION_STATS ? _SVP_PERIODE_ACTUALISATION_STATS : 0;
}

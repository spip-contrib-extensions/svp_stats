<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet de mettre à jour les statistiques d'utilisation des plugins.
 *
 * Elle ne nécessite aucun argument et est accessible qu'aux webmestres.
 *
 * @uses svp_actualiser_stats()
 *
 * @return void
 */
function action_actualiser_stats_dist() {
	// Securisation: aucun argument attendu, mais étant donné le bug de la balise
	// #URL_ACTION_AUTEUR il est préférable d'en passer un : on fait donc ça le plus
	// proprement possible en passant l'argument "tout".
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$mode = $securiser_action();

	// Verification des autorisations
	if (!autoriser('webmestre')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Actualisation des statistiques d'utilisation des plugins en provenance de
	// stats.spip.org
	// On verife tout de meme qu'il y a au moins un depot
	if (
		($mode === 'tout')
		and sql_countsel('spip_depots')
	) {
		include_spip('inc/svp_statistiquer');
		svp_actualiser_stats();
		// On consigne l'action
		spip_log('MODULE STATS - ACTION ACTUALISER STATS (manuel)', 'svpbase.' . _LOG_INFO);
	}
}

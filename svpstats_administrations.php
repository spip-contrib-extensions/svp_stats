<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function svpstats_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_plugins', 'spip_plugins_stats']]
	];

	// On supprime id_plugin au profit de prefixe plus pérenne et
	// on rajoute des champs pour l'historique.
	$maj['0.2'] = [
		['maj02_svpstats']
	];

	$maj['1'] = [
		['sql_alter', "TABLE spip_plugins_stats CHANGE prefixe prefixe VARCHAR(48) DEFAULT '' NOT NULL"]
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function svpstats_vider_tables(string $nom_meta_base_version) : void {
	// Supprimer les colonnes ajoutées à la table spip_plugins
	sql_alter('TABLE spip_plugins DROP COLUMN nbr_sites');
	sql_alter('TABLE spip_plugins DROP COLUMN popularite');

	// Supprimer la table de stats ajoutée
	sql_drop_table('spip_plugins_stats');

	// Supprimer la meta du schéma du plugin
	effacer_meta($nom_meta_base_version);
}

/**
 * Migration du schéma 0.1 au 0.2.
 *
 * Suppression de l'id_plugin remplacé par le prfixe du plugin plus pérenne.
 * Ajout d'un champ historique (tableau srialisé des valeurs de chaque mois)
 * et du timestamp.
 * Aucune sauvegarde n'est à faire car cette table n'était pas encore utilisée.
 *
 * @return void
 */
function maj02_svpstats() {
	sql_alter("TABLE spip_plugins_stats ADD prefixe varchar(30) DEFAULT '' NOT NULL AFTER id_plugin");
	sql_alter('TABLE spip_plugins_stats DROP COLUMN id_plugin');
	sql_alter("TABLE spip_plugins_stats ADD historique text DEFAULT '' NOT NULL AFTER popularite");
	sql_alter('TABLE spip_plugins_stats ADD maj TIMESTAMP');
}

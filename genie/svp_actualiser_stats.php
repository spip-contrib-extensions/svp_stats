<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ce CRON permet mettre à jour les statistiques d'utilisation des plugins.
 *
 * @uses svp_actualiser_stats()
 *
 * @param int $last Timestamp de la date de dernier appel de la tâche.
 *
 * @return int Retour de la fonction toujours à 1
 */
function genie_svp_actualiser_stats_dist(int $last) : int {
	// On recupere en base de donnees les statistiques d'utilisation des plugins
	// L'action n'est lancee que si il existe au moins un depot en base
	if (sql_countsel('spip_depots')) {
		include_spip('inc/svp_statistiquer');
		svp_actualiser_stats();
		spip_log('MODULE STATS - ACTION ACTUALISER STATS (automatique)', 'svpbase.' . _LOG_INFO);
	}

	return 1;
}
